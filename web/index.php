<?php
/**
 * Created by PhpStorm.
 * User: maciej
 * Date: 23.03.17
 * Time: 16:15
 */

use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManager;


require_once __DIR__.'/../vendor/autoload.php';

$app = new Silex\Application();
$app->register(new Silex\Provider\TwigServiceProvider(), array(
        'twig.path' => __DIR__.'/views'
));
$app['debug'] = true;

$isDevMode = true;
$config = Setup::createAnnotationMetadataConfiguration(array(__DIR__."/src", $isDevMode));

$conn = array(
    'user' => 'root',
    'password' => '111111',
    'database' => 'nauka',
    'driver' => 'pdo_sqlite',
    'path' => __DIR__.'/db_sqlite',
);

$entityManager = EntityManager::create($conn, $config);

$news = new \myNewApp\Controller\news('tytul', 'jakas data', 'jakis content');
$entityManager->persist($news);
$entityManager->flush();

echo "sp[rawdzenie czy toto dziala";


//$app->get("/", "myNewApp\\Controller\\UserController::index");
//$app->get("/get", "myNewApp\\Controller\\contentController::getIndex");
//$app->get("/put/{title}/{date}/{content}", "myNewApp\\Controller\\contentController::putIndex");

$app->run();


?>