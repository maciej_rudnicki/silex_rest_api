<?php
/**
 * Created by PhpStorm.
 * User: maciej
 * Date: 23.03.17
 * Time: 18:41
 */

namespace myNewApp\Controller;


class news
{
    protected $title;
    protected $date;
    protected $content;

    public function __construct($title, $date, $content)
    {
        $this->title = $title;
        $this->date = $date;
        $this->content = $content;
    }

    public function __destruct()
    {
        // TODO: Implement __destruct() method.
    }

    public function setTitle($title){
        $this->title = $title;
    }

    public function getTitle(){
        return $this->title;
    }

    public function setDate($date){
        $this->date = $date;
    }

    public function getDate(){
        return $this->date;
    }

    public function setContent($content){
        $this->content = $content;
    }

    public function getContent(){
        return $this->content;
    }


    public function convert2array(){
        return array(
            'title' => $this->getTitle(),
            'date' => $this->getDate(),
            'content' => $this->getContent()
        );
    }

}