<?php
/**
 * Created by PhpStorm.
 * User: maciej
 * Date: 23.03.17
 * Time: 16:55
 */

namespace myNewApp\Controller;


use Silex\Application;

class UserController
{
    public function index(Application $app){

        return $app['twig']->render('base.html.twig');

    }
}