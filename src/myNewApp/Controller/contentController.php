<?php
/**
 * Created by PhpStorm.
 * User: maciej
 * Date: 23.03.17
 * Time: 19:30
 */

namespace myNewApp\Controller;


use Doctrine\DBAL\Configuration;
use Doctrine\DBAL\DriverManager;

use myNewApp\Controller\dbController;
use Silex\Application;
use Symfony\Component\HttpFoundation\Response;

class contentController
{
    public function getIndex(Application $app){
        $dbObject = new dbController();
        $config = new Configuration();
        $connParam = $dbObject->connectionParams();

        $conn = DriverManager::getConnection($connParam, $config);

        $sql = "SELECT * FROM news";

        $stmt = $conn->query($sql);

        $data = $stmt->fetchAll();


        return new Response(json_encode($data));
    }

    public function putIndex($title, $date, $content){
        $dbObject = new dbController();
        $config = new Configuration();
        $connParam = $dbObject->connectionParams();
        $conn = DriverManager::getConnection($connParam, $config);

        $sql = "INSERT INTO news VALUE('', '$title', '$date', '$content')";
        if ($stmt = $conn->query($sql) == true){
            return new Response("udało się dodać do bazy");
        }

    }
}