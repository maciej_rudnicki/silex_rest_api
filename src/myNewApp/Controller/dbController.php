<?php
/**
 * Created by PhpStorm.
 * User: maciej
 * Date: 23.03.17
 * Time: 17:45
 */

namespace myNewApp\Controller;


use Symfony\Component\HttpFoundation\Response;

class dbController
{
    private $dbname;
    private $user;
    private $password;
    private $host;
    private $driver;

    public function __construct($dbname = 'nauka', $user = 'root', $password = '111111', $host = 'localhost', $driver = 'pdo_mysql')
    {
        $this->dbname = $dbname;
        $this->user = $user;
        $this->password = $password;
        $this->host = $host;
        $this->driver = $driver;
    }
    public function getInfo($info){
        return $this->$info;
    }


    public function connectionParams(){
        return array(
            'dbname' => $this->getInfo(dbname),
            'user' => $this->getInfo(user),
            'password' => $this->getInfo(password),
            'host' => $this->getInfo(host),
            'driver' => $this->getInfo(driver),
        );
    }
}